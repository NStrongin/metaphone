﻿
namespace Metaphone
{
    /// <summary>
    /// Provides methods for the reading data for coding
    /// </summary>
    public interface ILoader
    {
        /// <summary>
        /// Get a word to coding
        /// </summary>
        /// <returns>The word</returns>
        public string GetWord();
        /// <summary>
        /// Get an array of words from file to coding
        /// </summary>
        /// <returns>The array of words</returns>
        public string[] LoadStrings(string path);
    }
}
