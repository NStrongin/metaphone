﻿using System;
using System.IO;

namespace Metaphone
{
    /// <summary>
    /// Class <c>Loader</c> provides methods for the reading data for coding
    /// </summary>
    public class Loader : ILoader
    {
        /// <summary>
        /// Reads the line of characters from the standard input stream.
        /// </summary>
        /// <returns>
        /// The next line of characters from the input stream, or null if no more lines are available.
        /// </returns>
        public string GetWord()
        {
            Console.WriteLine("Input word");
            return Console.ReadLine();
        }
        /// <summary>
        /// Gets array of words from text file.
        /// </summary>
        /// <param name="path">
        /// The full path for text file with type. If the path is empty, then the program will look for "text.txt" in current dirrectory.
        /// Example [D:\Sigma\Metaphone\Metaphone\text.txt]
        /// </param>
        /// <returns>The array of words from text file</returns>
        public string[] LoadStrings(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                path = System.IO.Directory.GetCurrentDirectory() + "\\text.txt";
            }
            try
            {
                return System.IO.File.ReadAllLines(path);
            }
            catch (Exception)
            {
                Console.WriteLine("Not find file");
            }
            return Array.Empty<string>();
        }
    }
}
