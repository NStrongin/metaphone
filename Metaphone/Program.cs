﻿using System;

namespace Metaphone
{
    class Program
    {
        static void Main(string[] args)
        {

            bool checkContinue = true;
            do
            {
                Console.Write("Chose:\n Input [L or l] to load string array from file or any other symbol to input word from console. " +
                    "If the path is empty, then the program will look for \"text.txt\" in current dirrectory. ");
                string inputTypeOfInput = Console.ReadLine();

                if (inputTypeOfInput == "L" || inputTypeOfInput == "l")
                {
                    Console.WriteLine("Input a path and a file with type:");
                    string path = Console.ReadLine();
                    string[] words = new Loader().LoadStrings(path);
                    foreach (var word in words)
                    {
                        Console.WriteLine(Metaphone.Coding(word));
                    }
                }
                else
                {
                    string word = new Loader().GetWord();
                    Console.WriteLine("Result: " + Metaphone.Coding(word));
                }

                Console.Write("Do you want to continue? (Input [F or f] to stop programm or any other symbol to continue.) ");
                string inputContinue = Console.ReadLine();

                if (inputContinue == "F" || inputContinue == "f")
                {
                    checkContinue = false;
                }
            } while (checkContinue);
        }
    }
}
