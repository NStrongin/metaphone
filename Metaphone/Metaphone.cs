﻿using System;
using System.Linq;
using System.Text;

namespace Metaphone
{
    /// <summary>
    ///  Class <c>Metaphone</c> encodes strings into a phonetic
    /// </summary>
    public class Metaphone
    {
        /// <summary>
        /// <value>Contain vowels</value>
        /// </summary>
        private static readonly char[] vowel = new char[] { 'a', 'e', 'i', 'o', 'u' };
        /// <summary>
        /// Encodes strings into a phonetic
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <returns>Encoded word</returns>
        public static string Coding(string inputeWord)
        {
            if (string.IsNullOrEmpty(inputeWord))
            { 
                return ""; 
            }
            StringBuilder outputWord = new("");

            int lastIndex = inputeWord.Length - 1;

            for (int i = 0; i < inputeWord.Length; i++)
            {

                if (i == 0)
                {

                    if (inputeWord.Length > 1) // check exception with 2 symbols
                    {
                        switch (inputeWord[0].ToString() + inputeWord[1].ToString())
                        {
                            case "kn":
                                outputWord.Append('N');
                                i++;
                                continue;

                            case "gn":
                                outputWord.Append('N');
                                i++;
                                continue;

                            case "pn":
                                outputWord.Append('N');
                                i++;
                                continue;

                            case "ac":
                                outputWord.Append('C');
                                i++;
                                continue;

                            case "wr":
                                outputWord.Append('R');
                                i++;
                                continue;

                            case "wh":
                                outputWord.Append('W');
                                i++;
                                continue;
                        }
                    }

                    if (inputeWord[0] == 'x')
                    {
                        outputWord.Append('S');
                        continue;
                    }

                    if (IsVowel(inputeWord[0]))
                    {
                        outputWord.Append(inputeWord[0].ToString().ToUpper());
                        continue;
                    }
                }

                switch (inputeWord[i])
                {
                    case 'b':
                        if (i == lastIndex && CheckPreviousCharacter(inputeWord, i, 'm'))
                        {
                            continue;
                        }
                        outputWord.Append('B');
                        break;

                    case 'c':
                        if (CheckNextCharacter(inputeWord, i, "ia".ToCharArray()))
                        { 
                            outputWord.Append('X');
                            break;
                        }

                        if (CheckNextCharacter(inputeWord, i, 'h'))
                        {
                            outputWord.Append('X');
                            break;
                        }

                        if (CheckNextCharacter(inputeWord, i, 'i') || CheckNextCharacter(inputeWord, i, 'e') || CheckNextCharacter(inputeWord, i, 'y'))
                        { 
                            outputWord.Append('S');
                            break;
                        }

                        outputWord.Append('K');
                        break;

                    case 'd':
                        if (CheckNextCharacter(inputeWord, i, "dgy".ToCharArray()) || CheckNextCharacter(inputeWord, i, "dge".ToCharArray()) || CheckNextCharacter(inputeWord, i, "dgi".ToCharArray()))
                        {
                            outputWord.Append('J');
                        }
                        else
                        { 
                            outputWord.Append('T'); 
                        }
                        break;

                    case 'f':
                        outputWord.Append('F');
                        break;

                    case 'g':
                        if (CheckNextCharacter(inputeWord, i, 'h') && IsCharactersBetweenVowel(inputeWord, i, i + 1))
                        {
                            continue;
                        }

                        if (CheckNextCharacter(inputeWord, i, 'n') || CheckNextCharacter(inputeWord, i, "ned".ToCharArray()))
                        {
                            continue;
                        }

                        if (CheckNextCharacter(inputeWord, i, 'i') || CheckNextCharacter(inputeWord, i, 'e') || CheckNextCharacter(inputeWord, i, 'y'))
                        {
                            outputWord.Append('J');
                            break;
                        }
                        outputWord.Append('K');
                        break;

                    case 'h':

                        if (CheckPreviousCharacterIsVowel(inputeWord, i) && !CheckNextCharacterIsVowel(inputeWord, i))
                        {
                            continue;
                        }
                        outputWord.Append('H');
                        break;

                    case 'j':
                        outputWord.Append('J');
                        break;

                    case 'k':
                        if (CheckPreviousCharacter(inputeWord, i, 'c'))
                        {
                            continue;
                        }
                        outputWord.Append('K');
                        break;

                    case 'l':
                        outputWord.Append('L');
                        break;

                    case 'm':
                        outputWord.Append('M');
                        break;

                    case 'n':
                        outputWord.Append('N');
                        break;

                    case 'p':

                        if (CheckNextCharacter(inputeWord, i, 'h'))
                        { 
                            outputWord.Append('F');
                            i++;
                            break;
                        }
                        outputWord.Append('P');
                        break;

                    case 'q':
                        outputWord.Append('K');
                        break;

                    case 'r':
                        outputWord.Append('R');
                        break;

                    case 's':

                        if (CheckNextCharacter(inputeWord, i, 'h') || CheckNextCharacter(inputeWord, i, "io".ToCharArray()) || CheckNextCharacter(inputeWord, i, "ia".ToCharArray()))
                        {
                            outputWord.Append('X');
                            break;
                        }
                        outputWord.Append('S');
                        break;

                    case 't':

                        if (CheckNextCharacter(inputeWord, i, "io".ToCharArray()) || CheckNextCharacter(inputeWord, i, "ia".ToCharArray()))
                        {
                            outputWord.Append('X');
                            break;
                        }

                        if (CheckNextCharacter(inputeWord, i, 'h'))
                        {
                            outputWord.Append('O');
                            break;
                        }

                        if (CheckNextCharacter(inputeWord, i, "tch".ToCharArray()))
                        {
                            continue;
                        }
                        outputWord.Append('T');
                        break;

                    case 'v':
                        outputWord.Append('F');
                        break;

                    case 'w':

                        if (CheckPreviousCharacterIsVowel(inputeWord, i))
                        {
                            outputWord.Append('W');
                        }
                        break;

                    case 'x':
                        outputWord.Append("KS");
                        break;

                    case 'y':

                        if (CheckPreviousCharacterIsVowel(inputeWord, i))
                        {
                            outputWord.Append('Y');
                        }
                        break;

                    case 'z':
                        outputWord.Append('S');
                        break;
                }
            }

            return outputWord.ToString();
        }
        /// <summary>
        /// Сheck if a character is vowel.
        /// </summary>
        /// <param name="symbol">The symbol to be checked</param>
        /// <returns>Result of checking</returns>
        private static bool IsVowel(char symbol) => vowel.Contains(symbol);
        /// <summary>
        /// This method сhecks if the previous character of the current character is equal to the given parameter.
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <param name="index">Index of current character</param>
        /// <param name="symbol">Given parameter</param>
        /// <returns>If the previous character is equal to the given parameter, then the method returns True.</returns>
        private static bool CheckPreviousCharacter(string inputeWord, int index, char symbol) => index > 0 && index < inputeWord.Length 
            ? inputeWord[index - 1] == symbol 
            : false;
        /// <summary>
        /// Checks if the next character of the current character is equal to the given parameter.
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <param name="index">Index of character that we checked</param>
        /// <param name="symbol">Given parameter</param>
        /// <returns>true if the next character is equel the given parameter; otherwise, false.</returns>
        private static bool CheckNextCharacter(string inputeWord, int index, char symbol) => index > 0 && index < inputeWord.Length - 1
            ? inputeWord[index + 1] == symbol
            : false;
        /// <summary>
        /// Checks for vowels between a set of characters.
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <param name="startIndex">Index of first character in set</param>
        /// <param name="endIndex">Index of last character in set</param>
        /// <returns>true if the vowels between the set of characters; otherwise, false.</returns>
        private static bool IsCharactersBetweenVowel(string inputeWord, int startIndex, int endIndex) => 
            startIndex > 0 && startIndex < endIndex && endIndex < inputeWord.Length - 1 
                ? CheckNextCharacterIsVowel(inputeWord, endIndex) && CheckPreviousCharacterIsVowel(inputeWord, startIndex) 
                : false;
        /// 
        /// <summary>
        ///     Checks if the next character of the current character is equal to the given parameter.
        /// </summary>
        /// 
        /// <param name="inputeWord">Word that will be encoded</param>
        /// 
        /// <param name="index">Index of character that we checked</param>
        /// 
        /// <param name="symbols">Set of characters</param>
        /// 
        /// <returns>true if the next character is equel the given parameter; otherwise, false.</returns>
        /// 
        private static bool CheckNextCharacter(string inputeWord, int index, char[] symbols)
        {
            bool check = false;

            if (index > 0 && index < inputeWord.Length - symbols.Length)
            {
                check = true;

                for (var i = 0; i < symbols.Length; i++)
                {
                    check &= inputeWord[index + i + 1] == symbols[i];
                }
            }
            
            return check;
        }
        /// <summary>
        /// Checks if the previous character of the current character is a vowel.
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <param name="index">Index of the current character that we checked</param>
        /// <returns>true if the previous character is a vowel; otherwise, false.</returns>
        private static bool CheckPreviousCharacterIsVowel(string inputeWord, int index) =>
            index > 1 && index < inputeWord.Length && vowel.Contains(inputeWord[index - 1]);
        /// <summary>
        /// Checks if the next character of the current character is a vowel.
        /// </summary>
        /// <param name="inputeWord">Word that will be encoded</param>
        /// <param name="index">Index of the current character that we checked</param>
        /// <returns>true if the next character is a vowel; otherwise, false.</returns>
        private static bool CheckNextCharacterIsVowel(string inputeWord, int index) =>
            index > 0 && index < inputeWord.Length && vowel.Contains(inputeWord[index + 1]);
    }
}
